package com.medicus.medicus.data.module.remote

data class BiomarkerRemoteModule (
    val id:Long?,
    val date:String?,
    val info:String?,
    val color:String?,
    val value:Int?,
    val symbol:String?,
    val insight:String?,
    val category:String?
)