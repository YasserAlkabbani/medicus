package com.medicus.medicus.data.data_source.local.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.medicus.medicus.data.data_source.local.BiomarkerDatabase
import com.medicus.medicus.data.module.database.BiomarkerDatabaseModule
import kotlinx.coroutines.flow.Flow

@Dao
interface BiomarkerDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertBiomarker(vararg biomarkerDatabase: BiomarkerDatabaseModule)

    @Query("SELECT * FROM biomarker_table")
    fun getBiomarkerList():Flow<List<BiomarkerDatabaseModule>>

    @Query("SELECT * FROM biomarker_table WHERE id=:biomarkerId")
    fun getSelectedBiomarker(biomarkerId: Long):Flow<BiomarkerDatabaseModule>

}