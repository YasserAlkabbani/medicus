package com.medicus.medicus.data.classes.seald_class

import androidx.navigation.NavDirections

sealed class NavigationManager {
    object NavigationManagerNothing : NavigationManager()
    object NavigationManagerPop : NavigationManager()
    class NavigationManagerToDestination(val navDirections: NavDirections,val currentDestination:Int):NavigationManager()
}