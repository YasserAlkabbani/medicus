package com.medicus.medicus.data.module.mapper

import com.medicus.medicus.data.module.database.BiomarkerDatabaseModule
import com.medicus.medicus.data.module.domain.BiomarkerDomainModule
import com.medicus.medicus.data.module.remote.BiomarkerRemoteModule
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

suspend fun BiomarkerDatabaseModule.mapperToDomain(): BiomarkerDomainModule {
    return withContext(Dispatchers.Default){
        BiomarkerDomainModule(
            id,date, info, darkColor,lightColor, value,symbol,insight,category
        )
    }
}