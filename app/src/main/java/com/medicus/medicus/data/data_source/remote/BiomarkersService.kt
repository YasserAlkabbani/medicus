package com.medicus.medicus.data.data_source.remote

import com.medicus.medicus.data.module.remote.BiomarkerRemoteModule
import retrofit2.http.GET
import retrofit2.http.Path

interface BiomarkersService {

    @GET("biomarkers/")
    suspend fun getBiomarkersList():List<BiomarkerRemoteModule>

    @GET("biomarkers/{biomarkerId}")
    suspend fun getSelectedBiomarker(
        @Path("biomarkerId") biomarkerId:Long
    ):BiomarkerRemoteModule

}