package com.medicus.medicus.data.repository

import com.medicus.medicus.data.classes.seald_class.RequestState
import com.medicus.medicus.data.classes.seald_class.getThrowableState
import com.medicus.medicus.data.data_source.local.dao.BiomarkerDao
import com.medicus.medicus.data.data_source.remote.BiomarkersService
import com.medicus.medicus.data.module.domain.BiomarkerDomainModule
import com.medicus.medicus.data.module.mapper.mapperToDatabase
import com.medicus.medicus.data.module.mapper.mapperToDomain
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.*
import javax.inject.Inject
import javax.inject.Singleton


@Singleton
class RealRepository @Inject constructor(
        private val biomarkersService: BiomarkersService,
        private val biomarkerDao: BiomarkerDao
    ) {

    suspend fun refreshBiomarkerList():Flow<RequestState<Any>>{
        return requestManger {
            biomarkersService.getBiomarkersList().map { it.mapperToDatabase()}.filter { it.id>-1 }.let { biomarkerDao.insertBiomarker(*it.toTypedArray()) }
        }
    }
    fun getBiomarkerList():Flow<List<BiomarkerDomainModule>>{
        return biomarkerDao.getBiomarkerList().map { it.map { it.mapperToDomain() } }
    }

    suspend fun refreshSelectedBiomarker(biomarkerId:Long):Flow<RequestState<Any>>{
        return requestManger { biomarkersService.getSelectedBiomarker(biomarkerId).mapperToDatabase().let {biomarkerDao.insertBiomarker(it)  } }
    }

    fun getSelectedBiomarker(biomarkerId:Long):Flow<BiomarkerDomainModule>{
        return biomarkerDao.getSelectedBiomarker(biomarkerId).map { it.mapperToDomain() }
    }
}

suspend fun <T>requestManger(requset:suspend ()->T): Flow<RequestState<T>> {
    return flow {
        emit(RequestState.RequestStateLoading)
        emit(RequestState.RequestStateSuccess(requset()))
    }.catch { throwable->emit(RequestState.RequestStateError(throwable.getThrowableState()))  }
        .onCompletion { delay(500); emit(RequestState.RequestStateNothing) }.flowOn(Dispatchers.IO)
}