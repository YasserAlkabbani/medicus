package com.medicus.medicus.data.module.domain

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class BiomarkerDomainModule (
    val id:Long,
    val date:String,
    val info:String,
    val darkColor:Int,
    val lightColor:Int,
    val value:Int,
    val symbol:String,
    val insight:String,
    val category:String
):Parcelable

fun getEmptyBiomarkerDomainModule():BiomarkerDomainModule{
    return BiomarkerDomainModule(-1,"","",0,0,0,"","","")
}