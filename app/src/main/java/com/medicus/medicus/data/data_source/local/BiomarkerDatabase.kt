package com.medicus.medicus.data.data_source.local

import androidx.room.Database
import androidx.room.RoomDatabase
import com.medicus.medicus.data.data_source.local.dao.BiomarkerDao
import com.medicus.medicus.data.module.database.BiomarkerDatabaseModule

@Database(entities = [BiomarkerDatabaseModule::class], version = 1,exportSchema = false)
abstract class BiomarkerDatabase : RoomDatabase() {
    abstract fun biomarkerDao(): BiomarkerDao
}