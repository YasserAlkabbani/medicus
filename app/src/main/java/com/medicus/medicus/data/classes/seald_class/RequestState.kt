package com.medicus.medicus.data.classes.seald_class

import retrofit2.HttpException
import java.io.IOException

sealed class RequestState<out T> {
    object RequestStateNothing:RequestState<Nothing>()
    object RequestStateLoading:RequestState<Nothing>()
    class RequestStateSuccess<T>(val result:T):RequestState<T>()
    class RequestStateError(val throwableState:ThrowableState):RequestState<Nothing>()
}

sealed class ThrowableState{
    abstract val message:String
    class ThrowableStateHttp(val code:Int, override val message: String):ThrowableState()
    class ThrowableStateIO(override val message:String):ThrowableState()
    class ThrowableStateOther(override val message: String):ThrowableState()
}

fun Throwable.getThrowableState():ThrowableState{
    return when(this){
        is HttpException ->{
            val message:String=when(code()){
                400-> getErrorMessage()
                401->getErrorMessage()
                406->getErrorMessage()
                422-> getErrorMessage()
                500->"Internal Server Error"
                else->getErrorMessage()
            }
            ThrowableState.ThrowableStateHttp(code(),message)
        }
        is IOException ->{ ThrowableState.ThrowableStateIO(message.orEmpty()) }
        else ->{ ThrowableState.ThrowableStateOther(message.orEmpty()) }
    }
}

fun HttpException.getErrorMessage():String{
    return this.response()?.errorBody()?.charStream()?.readText().orEmpty().unicodeDecode()
}

fun String.unicodeDecode(): String {
    val stringBuffer = StringBuilder()
    var i = 0
    while (i < this.length) {
        if (i + 1 < this.length)
            if (this[i].toString() + this[i + 1].toString() == "\\u") {
                val symbol = this.substring(i + 2, i + 6)
                val c = Integer.parseInt(symbol, 16)
                stringBuffer.append(c.toChar())
                i += 5
            } else stringBuffer.append(this[i])
        i++
    }
    return stringBuffer.toString()
}