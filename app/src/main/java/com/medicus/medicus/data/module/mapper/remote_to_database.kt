package com.medicus.medicus.data.module.mapper

import android.graphics.Color
import androidx.core.graphics.ColorUtils
import com.medicus.medicus.data.module.database.BiomarkerDatabaseModule
import com.medicus.medicus.data.module.domain.BiomarkerDomainModule
import com.medicus.medicus.data.module.remote.BiomarkerRemoteModule
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import timber.log.Timber
import kotlin.math.roundToInt

suspend fun BiomarkerRemoteModule.mapperToDatabase():BiomarkerDatabaseModule{
    return withContext(Dispatchers.Default){
        BiomarkerDatabaseModule(
            id?:-1,date?:"Empty Data", info?:"Empty Info",
            color.getIntColorFromString(),color.getIntColorFromString().getLighterColor(),
            value?:0,symbol?:"Empty",insight?:"Empty Insight",category?:"Empty Category"
        )
    }
}

fun String?.getIntColorFromString():Int{
    return Color.parseColor(this?:"#AAAAAAAA")
}
fun Int.getLighterColor():Int{
    return ColorUtils.blendARGB(this, Color.WHITE, 0.8f)
}