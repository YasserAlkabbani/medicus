package com.medicus.medicus.data.module.database

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity(tableName= "biomarker_table")
data class BiomarkerDatabaseModule(
    @PrimaryKey(autoGenerate = true) val id:Long,
    @ColumnInfo(name="date") val date:String,
    @ColumnInfo(name="info") val info:String,
    @ColumnInfo(name="dark_color") val darkColor:Int,
    @ColumnInfo(name="light_color") val lightColor:Int,
    @ColumnInfo(name="value") val value:Int,
    @ColumnInfo(name="symbol") val symbol:String,
    @ColumnInfo(name="insight") val insight:String,
    @ColumnInfo(name="category") val category:String
)