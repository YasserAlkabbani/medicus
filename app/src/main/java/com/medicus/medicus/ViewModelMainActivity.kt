package com.medicus.medicus

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import com.medicus.medicus.data.classes.seald_class.NavigationManager
import com.medicus.medicus.data.classes.seald_class.RequestState
import com.medicus.medicus.data.repository.RealRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import javax.inject.Inject

@HiltViewModel
class ViewModelMainActivity @Inject constructor(
    private val application: Application,
    private val savedStateHandle: SavedStateHandle,
    private val realRepository: RealRepository
):ViewModel() {

    private val _navigationManager: MutableStateFlow<NavigationManager> = MutableStateFlow(NavigationManager.NavigationManagerNothing)
    val navigationManager: StateFlow<NavigationManager> =_navigationManager
    fun setNavigationManager(newNavigationManager: NavigationManager){_navigationManager.value=newNavigationManager}

    private val _requestState:MutableStateFlow<RequestState<Any>> = MutableStateFlow(RequestState.RequestStateNothing)
    val requestState:StateFlow<RequestState<Any>> =_requestState
    fun setRequestState(newRequestState: RequestState<Any>){_requestState.value=newRequestState}

}