package com.medicus.medicus.ui.biomarker_details

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.medicus.medicus.R
import com.medicus.medicus.ViewModelMainActivity
import com.medicus.medicus.data.classes.seald_class.RequestState
import com.medicus.medicus.databinding.DialogBiomarkerInfoBinding
import com.medicus.medicus.databinding.FragmentBiomarkerDetailsBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.filterNot

@AndroidEntryPoint
class BiomarkerDetailsFragment : Fragment() {

    private val viewModelMainActivity: ViewModelMainActivity by activityViewModels()
    private val viewModelBiomarkerDetails:ViewModelBiomarkerDetails by viewModels()
    private lateinit var fragmentBiomarkerDetailsBinding: FragmentBiomarkerDetailsBinding

    private lateinit var bottomSheetDialog: BottomSheetDialog
    private lateinit var dialogBiomarkerInfoBinding: DialogBiomarkerInfoBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        fragmentBiomarkerDetailsBinding=FragmentBiomarkerDetailsBinding.inflate(inflater,container,false)
        fragmentBiomarkerDetailsBinding.viewModelBiomarkerDetails=viewModelBiomarkerDetails
        fragmentBiomarkerDetailsBinding.lifecycleOwner=viewLifecycleOwner


        setInfoDialog(container)
        setRequestState()

        return fragmentBiomarkerDetailsBinding.root
    }

    fun setInfoDialog(container: ViewGroup?){
        if (!::bottomSheetDialog.isInitialized&&!::dialogBiomarkerInfoBinding.isInitialized){
            dialogBiomarkerInfoBinding=DialogBiomarkerInfoBinding.inflate(layoutInflater,container,false)
            dialogBiomarkerInfoBinding.lifecycleOwner=viewLifecycleOwner
            bottomSheetDialog= BottomSheetDialog(requireContext())
            bottomSheetDialog.setContentView(dialogBiomarkerInfoBinding.root)
            bottomSheetDialog.setOnShowListener {
                bottomSheetDialog.findViewById<View>(R.id.design_bottom_sheet)?.setBackgroundResource(android.R.color.transparent)
                viewModelBiomarkerDetails.setDialogState(true)
            }
            bottomSheetDialog.setOnDismissListener { viewModelBiomarkerDetails.setDialogState(false) }
        }

        lifecycleScope.launchWhenResumed {
            viewModelBiomarkerDetails.infoDialog.collect {
                dialogBiomarkerInfoBinding.biomarkerDomainModule=viewModelBiomarkerDetails.selectedBiomarker
                if (it&&!bottomSheetDialog.isShowing)bottomSheetDialog.show()
                else if (!it&&bottomSheetDialog.isShowing)bottomSheetDialog.dismiss()
            }
        }
    }

    fun setRequestState(){
        lifecycleScope.launchWhenResumed {
            viewModelBiomarkerDetails.requestState.collect {
                viewModelMainActivity.setRequestState(it)
            }
        }
    }

}