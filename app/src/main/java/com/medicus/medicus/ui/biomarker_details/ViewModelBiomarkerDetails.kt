package com.medicus.medicus.ui.biomarker_details

import android.app.Application
import androidx.lifecycle.*
import com.medicus.medicus.data.classes.seald_class.RequestState
import com.medicus.medicus.data.module.domain.BiomarkerDomainModule
import com.medicus.medicus.data.module.domain.getEmptyBiomarkerDomainModule
import com.medicus.medicus.data.repository.RealRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.inject.Inject

@HiltViewModel
class ViewModelBiomarkerDetails @Inject constructor(
    private val application: Application,
    private val savedStateHandle:SavedStateHandle,
    private val realRepository: RealRepository
):ViewModel() {

    val selectedBiomarker=savedStateHandle.get<BiomarkerDomainModule>("biomarkerDomainModule")!!

    val biomarkerDomainModuleLiveData:LiveData<BiomarkerDomainModule> =realRepository.getSelectedBiomarker(selectedBiomarker.id).asLiveData()

    private val _requestState:MutableStateFlow<RequestState<Any>> = MutableStateFlow(RequestState.RequestStateNothing)
    val requestState: StateFlow<RequestState<Any>> =_requestState
    val requestStateLiveData:LiveData<RequestState<Any>> =_requestState.asLiveData()
    fun setRequestState(newRequestState: RequestState<Any>){_requestState.value=newRequestState}

    init {
        refreshBiomarkerDetails()
    }

    fun refreshBiomarkerDetails(){
        viewModelScope.launch {
            realRepository.refreshSelectedBiomarker(selectedBiomarker.id).collect {
                setRequestState(it)
            }
        }
    }

    private val _infoDialog:MutableStateFlow<Boolean> = MutableStateFlow(false)
    val infoDialog:StateFlow<Boolean> =_infoDialog
    fun setDialogState(newInfoDialogState:Boolean){_infoDialog.value=newInfoDialogState}

}