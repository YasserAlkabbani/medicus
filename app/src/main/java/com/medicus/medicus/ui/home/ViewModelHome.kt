package com.medicus.medicus.ui.home

import android.app.Application
import androidx.lifecycle.*
import com.medicus.medicus.R
import com.medicus.medicus.data.classes.seald_class.NavigationManager
import com.medicus.medicus.data.classes.seald_class.RequestState
import com.medicus.medicus.data.module.domain.BiomarkerDomainModule
import com.medicus.medicus.data.repository.RealRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.inject.Inject

@HiltViewModel
class ViewModelHome @Inject constructor(
    private val application: Application,
    private val savedStateHandle: SavedStateHandle,
    private val realRepository: RealRepository
):ViewModel() {

    private val _navigationManager:MutableStateFlow<NavigationManager> = MutableStateFlow(NavigationManager.NavigationManagerNothing)
    val navigationManager:StateFlow<NavigationManager> =_navigationManager
    fun setNavigationManager(newNavigationManager: NavigationManager){_navigationManager.value=newNavigationManager}

    val biomarkerList:Flow<List<BiomarkerDomainModule>> = realRepository.getBiomarkerList()

    private val _requestState:MutableStateFlow<RequestState<Any>> = MutableStateFlow(RequestState.RequestStateNothing)
    val requestState:StateFlow<RequestState<Any>> =_requestState
    val requestStateLiveData:LiveData<RequestState<Any>> =_requestState.asLiveData()
    fun setRequestState(newRequestState:RequestState<Any>){_requestState.value=newRequestState}


    init {
        refreshBiomarkerList()
    }

    fun refreshBiomarkerList(){
        viewModelScope.launch {
            realRepository.refreshBiomarkerList().collect {
                setRequestState(it)
            }
        }
    }

    fun setNavigateToBiomarker(biomarkerDomainModule:BiomarkerDomainModule){
        setNavigationManager(
            NavigationManager.NavigationManagerToDestination(
                HomeFragmentDirections.actionHomeFragmentToBiomarkerDetailsFragment(biomarkerDomainModule,biomarkerDomainModule.symbol),R.id.homeFragment
            )
        )
    }

}