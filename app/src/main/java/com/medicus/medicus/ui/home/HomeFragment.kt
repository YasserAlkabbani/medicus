package com.medicus.medicus.ui.home

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.medicus.medicus.BiomarkerRecyclerViewAdapter
import com.medicus.medicus.BiomarkerRecyclerViewAdapterClickListener
import com.medicus.medicus.ViewModelMainActivity
import com.medicus.medicus.data.classes.seald_class.NavigationManager
import com.medicus.medicus.data.classes.seald_class.RequestState
import com.medicus.medicus.databinding.FragmentHomeBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.filterNot

@AndroidEntryPoint
class HomeFragment : Fragment() {

    private lateinit var biomarkerRecyclerViewAdapter:BiomarkerRecyclerViewAdapter
    private lateinit var fragmentHomeBinding: FragmentHomeBinding
    private val viewModelHome: ViewModelHome by viewModels()
    private val viewModelMainActivity:ViewModelMainActivity by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        fragmentHomeBinding=FragmentHomeBinding.inflate(inflater,container,false)
        fragmentHomeBinding.homeViewModel=viewModelHome
        fragmentHomeBinding.lifecycleOwner=viewLifecycleOwner

        if (::biomarkerRecyclerViewAdapter.isLateinit){
            biomarkerRecyclerViewAdapter= BiomarkerRecyclerViewAdapter(biomarkerRecyclerViewAdapterClickListener =
                BiomarkerRecyclerViewAdapterClickListener(
                    click = {viewModelHome.setNavigateToBiomarker(it)}
                )
            )
        }
        fragmentHomeBinding.biomarkersRecyclerView.adapter=biomarkerRecyclerViewAdapter

        lifecycleScope.launchWhenResumed {
            viewModelHome.biomarkerList.collect {
                biomarkerRecyclerViewAdapter.submitList(it)
            }
        }

        setNavigationManager()
        setRequestState()

        return fragmentHomeBinding.root
    }

    fun setNavigationManager(){
        lifecycleScope.launchWhenResumed {
            viewModelHome.navigationManager.filterNot { it is NavigationManager.NavigationManagerNothing }.collect {
                viewModelHome.setNavigationManager(NavigationManager.NavigationManagerNothing)
                viewModelMainActivity.setNavigationManager(it)
            }
        }
    }
    fun setRequestState(){
        lifecycleScope.launchWhenResumed {
            viewModelHome.requestState.collect {
                viewModelMainActivity.setRequestState(it)
            }
        }
    }
}