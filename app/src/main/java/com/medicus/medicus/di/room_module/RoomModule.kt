package com.medicus.medicus.di.room_module

import android.app.Application
import androidx.room.Room
import com.medicus.medicus.data.data_source.local.BiomarkerDatabase
import com.medicus.medicus.data.data_source.local.dao.BiomarkerDao
import com.medicus.medicus.data.module.database.BiomarkerDatabaseModule
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
object RoomModule {
    @Singleton
    @Provides
    fun provideBiomarkerDatabase(application: Application):BiomarkerDatabase{
            return Room.databaseBuilder(
            application.applicationContext,
            BiomarkerDatabase::class.java, "biomarker_database"
        ).build()
    }
    @Singleton
    @Provides
    fun provideBiomarkerDao(biomarkerDatabase: BiomarkerDatabase):BiomarkerDao{
        return biomarkerDatabase.biomarkerDao()
    }
}