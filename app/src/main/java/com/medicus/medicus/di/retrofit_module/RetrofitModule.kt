package com.medicus.medicus.di.retrofit_module

import com.medicus.medicus.data.data_source.remote.BiomarkersService
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object RetrofitModule {
    @Singleton
    @Provides
    fun provideRetrofitService():BiomarkersService{

    val moshi=Moshi.Builder().add(KotlinJsonAdapterFactory()).build()
    val okHttpClient:OkHttpClient= OkHttpClient().newBuilder()
        .callTimeout(60, TimeUnit.SECONDS)
        .connectTimeout(60, TimeUnit.SECONDS)
        .readTimeout(60, TimeUnit.SECONDS)
        .writeTimeout(60, TimeUnit.SECONDS)
        .build()

        return Retrofit.Builder()
            .baseUrl("https://retoolapi.dev/hZZ5j8/")
            .client(okHttpClient)
            .addConverterFactory(MoshiConverterFactory.create(moshi))
            .build()
            .create(BiomarkersService::class.java)
    }

}

