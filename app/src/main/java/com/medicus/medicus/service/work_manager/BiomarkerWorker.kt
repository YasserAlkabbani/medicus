package com.medicus.medicus.service.work_manager

import android.content.Context
import androidx.hilt.work.HiltWorker
import androidx.work.CoroutineWorker
import androidx.work.Worker
import androidx.work.WorkerParameters
import com.medicus.medicus.data.classes.seald_class.RequestState
import com.medicus.medicus.data.module.mapper.mapperToDatabase
import com.medicus.medicus.data.repository.RealRepository
import dagger.assisted.Assisted
import dagger.assisted.AssistedInject
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.filterIsInstance
import retrofit2.HttpException
import timber.log.Timber

@HiltWorker
class BiomarkerWorker @AssistedInject constructor(
    @Assisted appContext: Context,
    @Assisted workerParams: WorkerParameters,
    val realRepository: RealRepository
) : CoroutineWorker(appContext, workerParams) {
    override suspend fun doWork(): Result {
        var result:Result= Result.retry()
        realRepository.refreshBiomarkerList().filterIsInstance<RequestState.RequestStateSuccess<Any>>().collect {
            result= Result.success()
        }
        return result
    }
}