package com.medicus.medicus

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.viewModels
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.lifecycleScope
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.setupWithNavController
import com.medicus.medicus.data.classes.seald_class.NavigationManager
import com.medicus.medicus.data.classes.seald_class.RequestState
import com.medicus.medicus.databinding.ActivityMainBinding
import com.medicus.medicus.utils.showToast
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.filterNot

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    private val viewModelMainActivity:ViewModelMainActivity by viewModels()
    private lateinit var navController:NavController

    private lateinit var activityMainBinding:ActivityMainBinding


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        activityMainBinding=DataBindingUtil.setContentView(this,R.layout.activity_main)
        activityMainBinding.viewModelMainActivity=viewModelMainActivity
        activityMainBinding.lifecycleOwner=this


        val navHostFragment = supportFragmentManager.findFragmentById(R.id.HomeNavHostFragment) as NavHostFragment
        navController = navHostFragment.navController

        activityMainBinding.mainToolbar.setupWithNavController(navController)

        setNavigationManager()
        setRequestState()

    }

    fun setNavigationManager(){
        lifecycleScope.launchWhenResumed {
            viewModelMainActivity.navigationManager.filterNot { it is NavigationManager.NavigationManagerNothing }.collect {
                viewModelMainActivity.setNavigationManager(NavigationManager.NavigationManagerNothing)
                when (it){
                    is NavigationManager.NavigationManagerPop->navController.popBackStack()
                    is NavigationManager.NavigationManagerToDestination -> {
                        if (navController.currentDestination?.id==it.currentDestination){
                            navController.navigate(it.navDirections)
                        }
                    }
                    NavigationManager.NavigationManagerNothing -> {}
                }
            }
        }
    }

    fun setRequestState(){
        lifecycleScope.launchWhenResumed {
            viewModelMainActivity.requestState.filterNot { it is RequestState.RequestStateNothing }.collect {
                when(it){
                    is RequestState.RequestStateSuccess->{showToast(getString(R.string.fetch_data_done_successfully))}
                    is RequestState.RequestStateError->{showToast(it.throwableState.message)}
                    RequestState.RequestStateLoading -> {}
                    RequestState.RequestStateNothing -> {}
                }
            }
        }
    }

}