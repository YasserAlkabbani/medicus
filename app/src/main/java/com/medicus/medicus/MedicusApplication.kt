package com.medicus.medicus

import android.app.Application
import android.os.Build
import androidx.hilt.work.HiltWorkerFactory
import androidx.work.*
import com.medicus.medicus.service.work_manager.BiomarkerWorker
import dagger.hilt.android.HiltAndroidApp
import timber.log.Timber
import java.util.concurrent.TimeUnit
import javax.inject.Inject

@HiltAndroidApp
class MedicusApplication:Application(), Configuration.Provider {

    @Inject lateinit var workerFactory: HiltWorkerFactory

    override fun onCreate() {
        super.onCreate()
        Timber.plant(Timber.DebugTree())
        Timber.d("MedicusApp StartApplication")
        startWorkManager()
    }

    override fun getWorkManagerConfiguration(): Configuration {
        return Configuration.Builder().setWorkerFactory(workerFactory).build()
    }

    fun startWorkManager(){
                val constraints = Constraints.Builder()
            .setRequiredNetworkType(NetworkType.CONNECTED)
            .setRequiresBatteryNotLow(true)
//                 .apply { if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) { setRequiresDeviceIdle(true) } }
            .build()

        val biomarkerWorkerRequest =
            PeriodicWorkRequestBuilder<BiomarkerWorker>(1,TimeUnit.HOURS)
                .setConstraints(constraints)
                .build()

        WorkManager.getInstance(this)
            .enqueueUniquePeriodicWork("RefreshBiomarker",ExistingPeriodicWorkPolicy.REPLACE,biomarkerWorkerRequest)

    }
}