package com.medicus.medicus

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.medicus.medicus.data.module.domain.BiomarkerDomainModule
import com.medicus.medicus.databinding.ItemCardBiomarkerBinding

class BiomarkerRecyclerViewAdapter(private val biomarkerRecyclerViewAdapterClickListener:BiomarkerRecyclerViewAdapterClickListener):
    ListAdapter<BiomarkerDomainModule,BiomarkerRecyclerViewAdapter.BiomarkerRecyclerViewAdapterViewHolder>
    (BiomarkerRecyclerViewAdapterDiffUtils()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BiomarkerRecyclerViewAdapterViewHolder {
        return BiomarkerRecyclerViewAdapterViewHolder.onCreateBiomarkerRecyclerViewAdapterViewHolder(parent)
    }

    override fun onBindViewHolder(holder: BiomarkerRecyclerViewAdapterViewHolder, position: Int) {
        holder.onBindBiomarkerRecyclerViewAdapterViewHolder(getItem(position),biomarkerRecyclerViewAdapterClickListener)
    }

    class BiomarkerRecyclerViewAdapterViewHolder private constructor(val itemCardBiomarkerBinding: ItemCardBiomarkerBinding) :
        RecyclerView.ViewHolder(itemCardBiomarkerBinding.root){
        companion object{
            fun onCreateBiomarkerRecyclerViewAdapterViewHolder(parent: ViewGroup):BiomarkerRecyclerViewAdapterViewHolder{
                val layoutInflater=LayoutInflater.from(parent.context)
                val itemCardBiomarkerBinding:ItemCardBiomarkerBinding=
                    DataBindingUtil.inflate(layoutInflater,R.layout.item_card_biomarker,parent,false)
                return BiomarkerRecyclerViewAdapterViewHolder(itemCardBiomarkerBinding)
            }
        }
        fun onBindBiomarkerRecyclerViewAdapterViewHolder(
            item: BiomarkerDomainModule,
            biomarkerRecyclerViewAdapterClickListener: BiomarkerRecyclerViewAdapterClickListener
        ){
            itemCardBiomarkerBinding.biomarkerDomainModule=item
            itemCardBiomarkerBinding.biomarkerRecyclerViewAdapterClickListener=biomarkerRecyclerViewAdapterClickListener
        }

    }

}

class BiomarkerRecyclerViewAdapterDiffUtils():DiffUtil.ItemCallback<BiomarkerDomainModule,>(){
    override fun areItemsTheSame(oldItem: BiomarkerDomainModule, newItem: BiomarkerDomainModule): Boolean {
        return oldItem.id==newItem.id
    }

    override fun areContentsTheSame(oldItem: BiomarkerDomainModule, newItem: BiomarkerDomainModule): Boolean {
        return oldItem==newItem
    }

}

class BiomarkerRecyclerViewAdapterClickListener(private val click:(BiomarkerDomainModule) ->Unit){
    fun onClick(biomarkerDomainModule:BiomarkerDomainModule){ click(biomarkerDomainModule) }
}